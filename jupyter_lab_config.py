# Configuration file for lab.
c.LabApp.collaborative = True
c.LabApp.expose_app_in_browser = False
c.LabApp.open_browser = False
c.ExtensionApp.open_browser = False
c.ServerApp.open_browser = False
c.ServerApp.allow_password_change = True

c.ServerApp.allow_root = True

c.ServerApp.certfile = '/ssl/certificate.crt'

# c.ServerApp.client_ca = ''
# c.ServerApp.config_file = ''


## The IP address the Jupyter server will listen on.
c.ServerApp.ip = '0.0.0.0'

c.ServerApp.keyfile = '/ssl/private.key'


#Set this if password is desired
c.ServerApp.password = ''

#Set this if a password should be required
c.ServerApp.password_required = False

c.ServerApp.port = 443

c.ServerApp.preferred_dir = '/jupyterlab/'


c.ServerApp.root_dir = '/jupyterlab'

# c.ServerApp.token = '<generated>'
