FROM debian:latest
RUN apt update && apt upgrade -y
RUN apt install -y python3 python3-pip python3-numpy \
	python3-numpy-dbg python3-matplotlib python3-matplotlib-dbg \
	python3-sklearn julia openssl nodejs npm wget
RUN pip install pandas jupyterlab ipywidgets
RUN julia -e 'using Pkg; Pkg.add.(["IJulia","Plots", "LinearAlgebra", "Makie"])'
RUN mkdir /ssl
RUN openssl req -x509 -sha256 -nodes -days 40 \
	-newkey rsa:4096 -keyout /ssl/private.key \
	-out /ssl/certificate.crt -subj "/C=/ST=/L=/CN=ownjupyterserver"
RUN mkdir jupyterlab

#Set configuration

COPY jupyter_lab_config.py /root/.jupyter/jupyter_lab_config.py
EXPOSE 80/tcp
EXPOSE 80/udp

EXPOSE 443/tcp
EXPOSE 443/udp
CMD jupyter-lab --allow-root --no-browser --collaborative
