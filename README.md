# Collaborative-Jupyter-Setup

Dockerfile to set up a jupyterlab instance with --colaborative enabled

## Start using docker-compose

Simply start the server by running `docker-compose -d up`


## Build and start the server manually using docker

Run `docker build . -t collaborative-jupyter-lab` to build an image called 
"collaborative-jupyter-lab"

After that you can run `docker run -d -p 443:443 collaborative-jupyter-lab` to start the server

## Configuring the Server

This repository contains a barebones configuration for a jupyter-lab instance.
To change the configuration just edit or replace the `jupyter_lab_config.py`
before building the container.

# License

The scripts and files in this repositoy are licensed under [CC0](https://creativecommons.org/publicdomain/zero/1.0/).

By contributing to this repository via a commit or a merged pull request you agree to license your additions under the [CC0](https://creativecommons.org/publicdomain/zero/1.0/) as well.
